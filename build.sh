#!/bin/bash

set -eoxu pipefail

if [[ -d fs ]]
then
	rm -r fs
fi

mkdir fs
cd fs
destdir=$(pwd)

make -C ../sbuild-work/glibc-2.34/build install DESTDIR=$destdir -j10
make -C ../sbuild-work/gcc-11.2.0/build install DESTDIR=$destdir -j10
make -C ../sbuild-work/gmp-6.2.1        install DESTDIR=$destdir -j10
make -C ../sbuild-work/mpc-1.2.1        install DESTDIR=$destdir -j10
make -C ../sbuild-work/mpfr-4.1.0       install DESTDIR=$destdir -j10
make -C ../sbuild-work/binutils-2.37    install DESTDIR=$destdir -j10
#make -C ../sbuild-work/tar-1.34         install DESTDIR=$destdir -j10
make -C ../sbuild-work/make-4.3         install DESTDIR=$destdir -j10

# Not GNU
make -C ../sbuild-work/zstd-1.5.0 PREFIX="/usr" LIBDIR="/lib64" DESTDIR="$destdir" install
make -C ../sbuild-work/linux-5.14.2 headers_install INSTALL_HDR_PATH=$destdir/usr


mkdir -p {dev,etc,home,lib,sbin}
mkdir -p {mnt,opt,proc,srv,sys}
mkdir -p var/{lib,lock,log,run,spool}
install -d -m 0750 root
install -d -m 1777 tmp
mkdir -p usr/{include,lib,share,src,bin,sbin}
mkdir -p bin
cp ../sbuild-work/busybox-1.33.1/busybox usr/bin/busybox

for util in $(./usr/bin/busybox --list-full); do
  # /usr/bin/strings is already from binutils
  if [[ ! -f $util ]]
  then
    ln -s /usr/bin/busybox $util
  fi
done

mkdir src
cp ../sbuild-work/*.tar.* src
cd src
tar xf tar-1.34.tar.xz
cd ..

#tar czf ../sickOS.tar.gz .
cd ..
podman build . -t sickos

