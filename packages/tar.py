name = "tar"
version = "1.34"
url = f"https://ftp.gnu.org/gnu/tar/tar-{version}.tar.xz"
build = [
    "CFLAGS=-O2 ./configure --prefix=/usr",
    "make -j$(nproc)",
]
