name = "gcc"
version = "11.2.0"
url = f"https://ftp.gnu.org/gnu/gcc/gcc-{version}/gcc-{version}.tar.xz"
build = [
    "mkdir build",
    "@cd build",
    "CFLAGS=-O2 CXXFLAGS=-O2 ../configure --prefix=/usr --disable-bootstrap --enable-languages=c",
    "make -j$(nproc)",
]
