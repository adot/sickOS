name = "glibc"
version = "2.34"
url = f"https://ftp.gnu.org/gnu/glibc/glibc-{version}.tar.xz"
build = [
    "mkdir build",
    "@cd build",
    "CFLAGS=-O2 CXXFLAGS=-O2 ../configure --prefix=/usr",
    "make -j$(nproc)",
]
