name = "zstd"
version = "1.5.0"
url = f"https://github.com/facebook/zstd/releases/download/v{version}/zstd-{version}.tar.gz"
build = ["make -j$(nproc)"]
