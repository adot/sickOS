name = "binutils"
version = "2.37"
url = f"https://ftp.gnu.org/gnu/binutils/binutils-{version}.tar.xz"
build = [
    "CFLAGS=-O2 ./configure --prefix=/usr",
    "make -j$(nproc)",
]
