name = "mpfr"
version ="4.1.0"
url = f"https://www.mpfr.org/mpfr-current/mpfr-{version}.tar.xz"
build = [
	"CFLAGS=-O2 ./configure --prefix=/usr",
	"make -j$(nproc)",
]
