name = "gmp"
version = "6.2.1"
url = f"https://gmplib.org/download/gmp/gmp-{version}.tar.lz"
build = ["./configure --prefix=/usr", "make -j$(nproc)"]
