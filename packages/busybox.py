name = "busybox"
version = "1.33.1"
url = f"https://busybox.net/downloads/busybox-{version}.tar.bz2"
build = [
    "make defconfig -j$(nproc)",
    'sed \'s/CONFIG_EXTRA_CFLAGS=""/CONFIG_EXTRA_CFLAGS="-I..\\/linux-headers\\/include"/\' -i .config',
    "sed 's/# CONFIG_STATIC is not set/CONFIG_STATIC=y/' -i .config",
    "make CC=musl-gcc -j$(nproc)",
]
