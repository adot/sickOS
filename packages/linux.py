name = "linux"
_version = [5, 14, 2]
version = ".".join(map(str, _version))
url = f"https://cdn.kernel.org/pub/linux/kernel/v{_version[0]}.x/linux-{version}.tar.xz"
build = [
    "make defconfig -j$(nproc)",
    'sed "s/=m/=y/" -i .config',
    "make headers_install INSTALL_HDR_PATH=../linux-headers",
    # "make -j$(nproc)",
]
