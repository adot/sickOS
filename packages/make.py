name = "make"
version = "4.3"
url = f"https://ftp.gnu.org/gnu/make/make-{version}.tar.lz"
build = [
    "CFLAGS=-O2 ./configure --prefix=/usr --without-guile",
    "make -j$(nproc)",
]
