name = "mpc"
version = "1.2.1"
url = f"https://ftp.gnu.org/gnu/mpc/mpc-{version}.tar.gz"
build = [
    "CFLAGS=-O2 ./configure --prefix=/usr",
    "make -j$(nproc)",
]
