import sys
from urllib.parse import urlparse
from pathlib import Path
import requests
import os

WORKDIR = Path("sbuild-work")

package = sys.argv[1]

if not WORKDIR.exists():
    os.mkdir(WORKDIR)

with open(package) as f:
    pkg_source = f.read()

locs = {}
exec(pkg_source, None, locs)

url = locs["url"]

tarname = Path(urlparse(url).path).name
file = Path("sbuild-work") / tarname
r = requests.get(url)
assert r.status_code == 200, r.content
# TODO: Enforce download succedes
with open(file, "wb") as f:
    f.write(r.content)
os.chdir(WORKDIR)
# TODO: check succedes
os.system(f"tar xf {tarname}")

os.chdir(f"{locs['name']}-{locs['version']}")
for i in locs['build']:
    if i.startswith("@cd "):
        os.chdir(i[4:])
    else:
        os.system(i)
